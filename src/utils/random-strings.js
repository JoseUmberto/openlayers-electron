let randomColor = ()=> {
  const colors = [
    "#7F3C8D",
    "#11A579",
    "#3969AC",
    "#F2B701",
    "#E73F74",
    "#80BA5A",
    "#E68310",
    "#008695",
    "#CF1C90",
    "#f97b72",
    "#4b4b8f",
    "#A5AA99"
  ]
  return colors[Math.floor(Math.random() * colors.length)];
}

let randomStyle = ()=> {
  return new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: randomColor(),
      width: 5
    })
  })
}

let generateID = ()=> {
  return '_' + Math.random().toString(36).substr(2, 9);
};

module.exports = {
  randomStyle,
  generateID
}