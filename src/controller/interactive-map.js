let config = require("./../config.js");
let MapAssembler = require("./map.js");
let Interactions = require("./interactions.js");

let assembled = new MapAssembler(config);

let interaction = new Interactions(assembled, config);

module.exports = {
  assembled,
  interaction
}