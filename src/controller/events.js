const interactiveMap = require("./interactive-map.js");
const random = require('./../utils/random-strings.js');
const cluster = require("./../model/models.js")
const geojson = new ol.format.GeoJSON()


//Refazer o events pq o this ta dando problema



function Events(){
  
  this.interaction = interactiveMap;
  
  this.draw = this.interaction.interaction.getDraw();
  this.modify = this.interaction.interaction.getModify();
  
  this.map = this.interaction.assembled.getMap();
  this.vectorLayer = interactiveMap.assembled.getVectorLayer();
  this.raster = interactiveMap.assembled.getRasterTile();
}


function verifyFeatures(feature){

  let geometry = feature.getGeometry();
  let source = interactiveMap.assembled.getVectorSource()

  if(source.getFeatures().length <= 0){
    return random.randomStyle();
  } else {
    let featuresVectorSource = source.getFeatures()
    let firstCoord;
    let lastCoord;
    for(let i = 0; i < featuresVectorSource.length; i++){

      let geometryVectorSource = featuresVectorSource[i].getGeometry();

      firstCoord = JSON.stringify(geometry.getFirstCoordinate());
      lastCoordVGS = JSON.stringify(geometryVectorSource.getLastCoordinate());
      firstCoordVGS = JSON.stringify(geometryVectorSource.getFirstCoordinate());

      if((firstCoord === lastCoordVGS) || (firstCoord === firstCoordVGS)){
        return featuresVectorSource[i].getStyle()
      }
    }
    return random.randomStyle();
  }

}

Events.prototype.Draw = function (event){
  //o this daqui é referencia ao evento do click que ocorre la no main.js
  this.interaction.interaction.init();

  this.draw.on('drawstart', function(drawStartEvent){
    drawStartEvent.feature.setId(random.generateID())
    let feature = drawStartEvent.feature;
    let style = verifyFeatures(feature);
    feature.setStyle(style)
  })

  this.draw.on('drawend', function(drawEndEvent) {
    let source = interactiveMap.assembled.getVectorSource();
    source.addFeature(drawEndEvent.feature);
    let feature = drawEndEvent.feature;
    
    cluster.featureCluster[feature.id_] = geojson.writeFeatureObject(feature, {
      dataProjection: 'EPSG:3857'
    })

    cluster.featureCluster[feature.id_]["style"] = {
      "color_": feature.getStyle().getStroke().getColor(),
      "width_": feature.getStyle().getStroke().getWidth()
    }
    
  });

  this.modify.on('modifystart', function(modifyStartEvent){    
  })

  this.modify.on('modifyend', function(modifyStartEvent){
  })

}

Events.prototype.LayerHandler = function(){

  let layerValue = document.querySelector('input[name="typeLayer"]:checked').value;

  if(layerValue !== "osm"){
    this.map.removeLayer(this.raster);
    this.map.removeLayer(this.vectorLayer)
    this.raster.setSource(new ol.source.BingMaps({
      key: "ciN5QAQYiHzOFNabIODf~b61cOBWqj2nmKSuoyjuyKA~AiShqLNGsToztBeSE2Tk8Pb1cUdr4nikxL24hlMRaHCJkIpKaYtdBXoxaDEgFhQv",
      imagerySet: "AerialWithLabels"
    }))
    this.map.addLayer(this.raster);
    this.map.addLayer(this.vectorLayer)
  } else {
    this.map.removeLayer(this.raster)
    this.map.removeLayer(this.vectorLayer)
    this.raster.setSource(new ol.source.OSM())
    this.map.addLayer(this.raster)
    this.map.addLayer(this.vectorLayer)
  }

}


module.exports = Events;