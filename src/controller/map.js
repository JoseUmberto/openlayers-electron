function MapAssembler(config){  
  this.osm = new ol.source.OSM();
  
  this.bing = new ol.source.BingMaps({
    key: config.bing.key,
    imagerySet: config.bing.imagerySet
  });

  this.vectorSource = new ol.source.Vector()
  
  this.raster = new ol.layer.Tile();
  
  this.style = new ol.style.Style();
  
  this.stroke = new ol.style.Stroke({
    color:config.stroke.color,
    width: config.stroke.width
  });

  this.style.setStroke(this.stroke);
  
  this.vectorLayer = new ol.layer.Vector({
    source: this.vectorSource,
    style: this.style
  });

  this.view = new ol.View({
    center: config.view.center,
    projection: config.view.projection,
    zoom: config.view.zoom
  })
  
  this.init();

}

MapAssembler.prototype.init = function(){

  this.raster.setSource(this.osm)

  this.map = new ol.Map({
    layers:[this.raster, this.vectorLayer],
    target: 'map',
    view: this.view
  })
}

MapAssembler.prototype.getMap = function(){
  return this.map;
}

MapAssembler.prototype.getVectorSource = function(){
  return this.vectorSource;
}


MapAssembler.prototype.getVectorLayer = function(){
  return this.vectorLayer;
}

MapAssembler.prototype.getRasterTile = function(){
  return this.raster;
}

module.exports = MapAssembler;