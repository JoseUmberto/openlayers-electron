function Interactions(assembled, config){

  this.map = assembled.getMap();

  this.draw = new ol.interaction.Draw({});

  this.modify = new ol.interaction.Modify({
    source: assembled.getVectorSource()
  });

  this.snap = new ol.interaction.Snap({
    source: assembled.getVectorSource()
  });

  this.draw = new ol.interaction.Draw({
    source: assembled.getVectorSource(),
    type: 'LineString',
    style: new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: config.drawDefinitions.color,
        lineDash: config.drawDefinitions.lineDash,
        width: config.drawDefinitions.width
      })
    })
  });
}

Interactions.prototype.init = function(){
  this.map.addInteraction(this.modify)
  this.map.addInteraction(this.draw)
  this.map.addInteraction(this.snap);
}

Interactions.prototype.getDraw = function(){
  return this.draw;
}

Interactions.prototype.getModify = function(){
  return this.modify;
}

module.exports = Interactions;