/*
var dialog = require('electron').remote.dialog;
var fs = require('fs');


const Events = require("./controller/events.js");

const events = new Events();

const form = document.getElementById("type")
const layer = document.getElementById("layer")
const importRoutes = document.getElementById("import-routes")
const exportRoutes = document.getElementById("export-routes") 

form.addEventListener('click', function(){ events.Draw() });
//importRoutes.addEventListener('click', importJsonHandling)
//exportRoutes.addEventListener('click', getJSON);
layer.addEventListener('click', function(){ events.LayerHandler() });

*/

var dialog = require('electron').remote.dialog;
var fs = require('fs');
var random = require('./utils/random-strings.js')

let featureAgregation = {};

const form = document.getElementById("type")
const layer = document.getElementById("layer")
const importJson = document.getElementById("import-routes")
const exportJSON = document.getElementById("export-routes")

const geojson = new ol.format.GeoJSON()

const raster = new ol.layer.Tile({
  source: new ol.source.OSM()
});

const source = new ol.source.Vector();

const vector = new ol.layer.Vector({
  source: source,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: '#ffcc33',
      width: 2
    })
  })
})
/*
var mousePositionControl = new ol.control.MousePosition({
  coordinateFormat: createStringXY(4),
  projection: 'EPSG:4326',
  // comment the following two lines to have the mouse position
  // be placed within the map.
  className: 'custom-mouse-position',
  target: document.getElementById('mouse-position'),
  undefinedHTML: '&nbsp;'
});
*/
const map = new ol.Map({
  layers: [raster, vector],
  target: 'map',
  view: new ol.View({
    center: [-49.2557858, -16.682363],
    projection: 'EPSG:4326',
    zoom: 15
  })
})

let draw = new ol.interaction.Draw({
  source: source,
  type: 'LineString',
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'rgba(0, 0, 0, 0.5)',
      lineDash: [10,10],
      width: 2
    })
  })
});

const modify = new ol.interaction.Modify({
  source: source
});

map.addInteraction(modify);

const snap = new ol.interaction.Snap({
  source: source
});

map.addInteraction(draw);
map.addInteraction(snap);

draw.setActive(false)

function verifyFeatures(feature){

  let geometry = feature.getGeometry();  
  if(source.getFeatures().length <= 0){
    return random.randomStyle();
  } else {
    let featuresVectorSource = source.getFeatures()
    let firstCoord;
    let lastCoord;
    for(let i = 0; i < featuresVectorSource.length; i++){

      let geometryVectorSource = featuresVectorSource[i].getGeometry();

      firstCoord = JSON.stringify(geometry.getFirstCoordinate());
      lastCoordVGS = JSON.stringify(geometryVectorSource.getLastCoordinate());
      firstCoordVGS = JSON.stringify(geometryVectorSource.getFirstCoordinate());

      if((firstCoord === lastCoordVGS) || (firstCoord === firstCoordVGS)){
        return featuresVectorSource[i].getStyle()
      }
    }
    return random.randomStyle();
  }
}

let drawInteraction = ()=>{
  draw.setActive(true)

  draw.on('drawstart', function(drawStartEvent){
    drawStartEvent.feature.setId(random.generateID())
    let feature = drawStartEvent.feature;

    let style = verifyFeatures(feature);
    feature.setStyle(style)
  })

  draw.on('drawend', function(drawEndEvent) {    
    source.addFeature(drawEndEvent.feature)
    let feature = drawEndEvent.feature;    
    featureAgregation[feature.id_] = geojson.writeFeatureObject(feature, {
      dataProjection: 'EPSG:4326'
    })

    featureAgregation[feature.id_]["style"] = {
      "color_": feature.getStyle().getStroke().getColor(),
      "width_": feature.getStyle().getStroke().getWidth()
    }
  });

  modify.on('modifystart', function(modifyStartEvent){    
  })

  modify.on('modifyend', function(modifyStartEvent){
  })
}


const formHandling = ()=>{
  let radioValue = document.querySelector('input[name="typeRadio"]:checked').value;
  drawInteraction()
}

function readFile(filepath) {
  fs.readFile(filepath, 'utf-8', function (err, data) {
    if(err){
      alert("An error ocurred reading the file :" + err.message);
      return;
    }
    let featuresFromFile = JSON.parse(data);

    for (var key in featuresFromFile) {
      if (featuresFromFile.hasOwnProperty(key)) {
        let coord = featuresFromFile[key].geometry.coordinates;
        let geometry = new ol.geom.LineString(coord);
        let featurething = new ol.Feature({
          name: key,
          geometry: geometry
        });
        let style = new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: featuresFromFile[key].style.color_,
            width: featuresFromFile[key].style.width_
          })
        })
        featurething.setStyle(style)
        source.addFeature(featurething)
      }
    }
  });
}

const importJsonHandling = () => {
  dialog.showOpenDialog(function (fileNames) {
    if(fileNames === undefined){
      console.log("No file selected");
    }else{
      readFile(fileNames[0]);
    }
  });
}

function getJSON(){
  let dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(featureAgregation));
  exportJSON.setAttribute("href", dataStr);
  exportJSON.setAttribute("download", "LineString.json");
  
}

const layerHandling = () => {
  let layerValue = document.querySelector('input[name="typeLayer"]:checked').value;
  let x = map.getLayers().getArray()
  for(let i = 0; i < x.length; i++){
    console.log('x[i]', x[i])    
  }
  if(layerValue !== "osm"){
    map.removeLayer(raster);
    map.removeLayer(vector)
    raster.setSource(new ol.source.BingMaps({
      key: "ciN5QAQYiHzOFNabIODf~b61cOBWqj2nmKSuoyjuyKA~AiShqLNGsToztBeSE2Tk8Pb1cUdr4nikxL24hlMRaHCJkIpKaYtdBXoxaDEgFhQv",
      imagerySet: "AerialWithLabels"
    }))
    map.addLayer(raster);
    map.addLayer(vector)
  } else {
    map.removeLayer(raster)
    map.removeLayer(vector)
    raster.setSource(new ol.source.OSM())
    map.addLayer(raster)
    map.addLayer(vector)
  }

}

form.addEventListener('click', formHandling);
importJson.addEventListener('click', importJsonHandling)
exportJSON.addEventListener('click', getJSON);
layer.addEventListener('click', layerHandling);

//Importante
//https://stackoverflow.com/questions/43474967/call-es5-class-method-from-static-method
