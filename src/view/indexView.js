const Events = require("./../controller/events.js");

function View (){

  const events = new Events();
  
  const form = document.getElementById("type")
  const layer = document.getElementById("layer")
  const importRoutes = document.getElementById("import-routes")
  const exportRoutes = document.getElementById("export-routes")  
  let layerValue = document.querySelector('input[name="typeLayer"]:checked');
  console.log("layerValue", layerValue)
  form.addEventListener('click', events.Draw());
  //importRoutes.addEventListener('click', importJsonHandling)
  //exportRoutes.addEventListener('click', getJSON);
  layer.addEventListener('click', events.LayerHandler());

}


module.exports = View;