module.exports = {
  "view": {
    center: [-5483129.009695, -1883881.331059],
    projection: "EPSG:3857",
    zoom: 15
  },
  "stroke": {
    color: "#ffcc33",
    width: 2
  },
  "bing": {
    key: "ciN5QAQYiHzOFNabIODf~b61cOBWqj2nmKSuoyjuyKA~AiShqLNGsToztBeSE2Tk8Pb1cUdr4nikxL24hlMRaHCJkIpKaYtdBXoxaDEgFhQv",
    imagerySet: "AerialWithLabels"
  },
  "drawDefinitions": {
    color:"rgba(0, 0, 0, 0.5)",
    lineDash: [10,10],
    width: 2
  }
}